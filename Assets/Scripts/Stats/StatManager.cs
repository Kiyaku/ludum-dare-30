﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StatManager : MonoBehaviour {
	public static StatManager ins;

	public List<Upgrade> AvailableUpgrades = new List<Upgrade>();

	private void Awake() {
		ins = this;
	}


	public Upgrade GetUpgradeByID(string ID) {
		return AvailableUpgrades.Find (u => u.ID == ID);
	}
}
