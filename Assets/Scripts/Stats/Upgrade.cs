﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Upgrade {
	public enum Categories {
		POWER,
		AGILITY,
		INTELLIGENCE
	}


	public string ID;
	public string Name;
	public string Description;
	public Categories Category;
	// TODO: Construct this string using the stat system later on
	public string StatChanges;
}
