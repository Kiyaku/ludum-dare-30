﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainMenu : MonoBehaviour {
	public static MainMenu ins;

	public enum MenuTypes {
		UPGRADE,
		JOURNAL,
		INVENTORY,
		OPTIONS,
		NULL
	}

	[System.Serializable]
	public class MenuButton {
		public UIToggle Button;
		public MenuTypes Type;
	}

	public List<SubMenu> SubMenus = new List<SubMenu>();
	public List<MenuButton> MenuButtons = new List<MenuButton>();

	private UIPanel panel;
	private bool isOpen = false;

	private MenuTypes lastType = MenuTypes.NULL;
	private SubMenu lastSubMenu;


	private void Awake() {
		ins = this;
		panel = GetComponent<UIPanel>();

		panel.alpha = 0;

		foreach(MenuButton button in MenuButtons) {
			MenuTypes type = button.Type;
			button.Button.optionCanBeNone = false;

			UIEventListener.Get (button.Button.gameObject).onClick = delegate(GameObject go) {
				OpenMenu(type);
			};
		}

		foreach(SubMenu sm in SubMenus) {
			sm.HidePanel();
		}
	}


	public void ToggleMenu() {
		if(isOpen)
			CloseMenu();
		else
			OpenMenu(lastType != MenuTypes.NULL ? lastType : MenuTypes.UPGRADE);
	}


	public void OpenMenu(MenuTypes type) {
		SubMenu newMenu = null;
		bool sameType = lastType == type;

		lastType = type;

		if(!isOpen) {
			Time.timeScale = 0;
			PanelManager.Ins.ShowPanel(panel);

			StopAllCoroutines();
			StartCoroutine(AnimateMenu(true));
		}

		// Find menu to open
		newMenu = SubMenus.Find (sm => sm.Type == type);
		int dir = lastSubMenu == null || MenuButtons.FindIndex(b => b.Type == lastSubMenu.Type) < MenuButtons.FindIndex(b => b.Type == newMenu.Type) ? -1 : 1;

		if(!isOpen || !sameType)
			newMenu.ShowPanel(isOpen && !sameType, dir);

		// Close old menu
		if(lastSubMenu != null && newMenu != null && lastSubMenu != newMenu) {
			lastSubMenu.HidePanel(isOpen && !sameType, dir);
		}

		lastSubMenu = newMenu;
		MenuButtons.Find(b => b.Type == type).Button.value = true;

		isOpen = true;
	}


	public void CloseMenu() {
		isOpen = false;

		Time.timeScale = 1;
		PanelManager.Ins.HidePanel(panel);

		StopAllCoroutines();
		StartCoroutine(AnimateMenu(false));

		foreach(SubMenu sm in SubMenus)
			sm.HidePanel();
	}


	private IEnumerator AnimateMenu(bool fadeIn) {
		float timer = 0;

		while(timer < 1) {
			timer += Time.unscaledDeltaTime * 5f;

			if(fadeIn) {
				panel.alpha = timer;
			} else {
				panel.alpha = (1f - timer);
			}

			yield return null;
		}

		if(fadeIn) {
			panel.alpha = 1;
		} else {
			panel.alpha = 0;
		}
	}
}
