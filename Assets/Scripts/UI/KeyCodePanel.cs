﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class KeyCodePanel : MonoBehaviour {
	public static KeyCodePanel ins;
	
	public List<UIButton> ButtonList = new List<UIButton>();
	public UIButton CloseButton;
	public UIButton BackButton;
	public UILabel PasswordLabel;
	public UILabel KeyCodeTitle;
	public string CorrectCode = "";
	public Color ColorCorrect;
	public Color ColorFalse;

	private bool acceptInput = true;
	private Color startColor;
	private Action callBack;


	private void Awake() {
		ins = this;

		transform.localPosition = Vector3.zero;
		startColor = PasswordLabel.color;

		if(ButtonList.Count < 10) {
			Debug.LogError("[KeyCodePanel] Not enough buttons assigned");
			return;
		}

		for(int i = 0; i < 10; i++) {
			int index = i;

			UIEventListener.Get (ButtonList[i].gameObject).onClick += delegate(GameObject go) {
				if(acceptInput)
					EnterNumber(index);
			};
		}

		UIEventListener.Get (CloseButton.gameObject).onClick += delegate(GameObject go) {
			ClosePanel();
		};

		UIEventListener.Get (BackButton.gameObject).onClick += delegate(GameObject go) {
			if(acceptInput && PasswordLabel.text.Length > 0) {
				PasswordLabel.text = PasswordLabel.text.Substring(0, PasswordLabel.text.Length - 1);
			}
		};
	}


	public void OpenPanel(string title, string code, Action callBack) {
		KeyCodeTitle.text = title;
		CorrectCode = code;
		this.callBack = callBack;
		PasswordLabel.text = "";
		PlayerControl.ins.ToggleMouseCursor(true);
	}


	public void ClosePanel() {
		PanelManager.Ins.HidePanel(transform.name);

		if(PlayerControl.ins != null)
			PlayerControl.ins.ToggleMouseCursor(false);
	}


	private void EnterNumber(int number) {
		PasswordLabel.text += number;

		if(PasswordLabel.text.Length >= 4) {
			// Validate
			ValidateCode();
		}
	}


	private void ValidateCode() {
		if(PasswordLabel.text == CorrectCode) {
			StartCoroutine(AnimateLamp(true));
		} else {
			StartCoroutine(AnimateLamp(false));
		}
	}


	private IEnumerator AnimateLamp(bool correct) {
		float timer = 0;

		acceptInput = false;
		PasswordLabel.color = correct ? ColorCorrect : ColorFalse;

		while(timer < 1) {
			if(!correct) {
				float alpha = Mathf.RoundToInt(Mathf.Abs (Mathf.Sin(timer * Mathf.Deg2Rad * 720f)));
				PasswordLabel.alpha = alpha;
			}

			timer += Time.deltaTime;

			yield return null;
		}

		if(!correct) {
			acceptInput = true;
			PasswordLabel.text = "";
			PasswordLabel.color = startColor;
		} else {
			if(callBack != null)
				callBack();

			ClosePanel();
		}

		PasswordLabel.alpha = 1;
	}
}
