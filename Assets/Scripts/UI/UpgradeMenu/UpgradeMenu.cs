﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpgradeMenu : SubMenu {
	public static UpgradeMenu ins;

	public PowerUpHexagon[] Hexagons;
	public UIButton UpgradeButton;
	public UILabel UpgradeButtonLabel;

	public UILabel UpgradeName;
	public UILabel UpgradeDesc;
	public UILabel UpgradeCost;
	public UISprite AccessDeniedSprite;

	private Hexagon tempHexagon;


	private void Awake() {
		ins = this;

		UIEventListener.Get (UpgradeButton.gameObject).onClick += delegate(GameObject go) {
			if(tempHexagon != null && tempHexagon.IsAvailable()) {
				tempHexagon.Unlock();
				DisplayInfo(tempHexagon, tempHexagon.InfoFontColor);
			}
		};

		ResetInfo();
	}


	public override void ShowPanel(bool animate = false, int dir = 1) {
		base.ShowPanel(animate, dir);

		foreach(PowerUpHexagon puh in Hexagons) {
			puh.ShowGroup();
		}
	}


	public override void HidePanel(bool animate = false, int dir = 1) {
		base.HidePanel(animate, dir);

		foreach(PowerUpHexagon puh in Hexagons) {
			puh.HideGroup();
		}
	}


	public void DeselectHexagons() {
		tempHexagon = null;
		DisplayInfo(null, Color.white);

		foreach(PowerUpHexagon puh in Hexagons)
			puh.DeselectHexagons();
	}


	private void ResetInfo() {
		UpgradeName.text = "No Upgrade Selected";
		UpgradeDesc.text = "";
		UpgradeCost.text = "";
		UpgradeButtonLabel.color = Color.white;
		UpgradeButton.gameObject.SetActive(false);
		AccessDeniedSprite.enabled = false;
	}
	

	public void DisplayInfo(Hexagon hexagon, Color fontColor, bool isSelected = false) {
		if(hexagon == null) {
			if(tempHexagon != null) {
				DisplayInfo(tempHexagon, tempHexagon.InfoFontColor);
				return;
			}

			ResetInfo();

			if(isSelected) {
				tempHexagon = null;
			}

			return;
		}

		if(isSelected) {
			tempHexagon = hexagon;
		}

		string upgradeID = hexagon.UpgradeID;
		Upgrade upgrade = StatManager.ins.GetUpgradeByID(upgradeID);

		if(upgradeID != null) {
			string colorStr = Helper.ColorToHex(fontColor);
			bool available = hexagon.IsAvailable();
			UpgradeButtonLabel.color = available ? fontColor : Color.gray;
			UpgradeButtonLabel.text = available ? "Install Upgrade" : "ACCESS DENIED";
			UpgradeName.text = upgrade.Name + " Upgrade";
			UpgradeDesc.text = "[i]\"" + upgrade.Description + "[-]\"\n\n[" + colorStr + "]" + upgrade.StatChanges + "[-]";
			UpgradeCost.text = "[" + colorStr + "]" + (hexagon.IsUnlocked ? "Installed" : "1 Upgrade Cube Required") + "[-]";
			UpgradeButton.gameObject.SetActive(!hexagon.IsUnlocked);
			AccessDeniedSprite.enabled = !available;
		}
	}
}
