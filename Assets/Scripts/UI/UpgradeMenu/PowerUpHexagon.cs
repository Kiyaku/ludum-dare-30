﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PowerUpHexagon : MonoBehaviour {
	public Vector3 SelectedPos;
	public List<Hexagon> Hexagons = new List<Hexagon>();
	public float StartDelay;

	private Vector3 startPos;
	private Vector3 targetPos;
	private Vector3 startScale;
	private bool isSelected = false;

	private UIWidget widget;
	private float targetAlpha;
	private float targetScale;

	private void Awake() {
		widget = GetComponent<UIWidget>();
		widget.alpha = 0;
		targetScale = 0;
		startScale = transform.localScale;
		startPos = transform.localPosition;
		targetPos = startPos;

		foreach(Transform child in transform) {
			Hexagon h = child.GetComponent<Hexagon>();

			if(h != null) {
				Hexagons.Add(h);
			}
		}
	}


	public void ShowGroup() {
		StartCoroutine(setAlpha(1, 0.2f));
	}


	public void HideGroup() {
		StartCoroutine(setAlpha(0, 0));
	}


	private IEnumerator setAlpha(float alpha, float additionalDelay) {
		float timer = 0;

		while(timer < StartDelay + additionalDelay) {
			timer += Time.unscaledDeltaTime;

			yield return null;
		}

		targetAlpha = alpha;
		targetScale = alpha;
	}


	public void Hover() {
		isSelected = true;
	}


	public void Exit() {
		isSelected = false;
	}


	public void DeselectHexagons() {
		foreach(Hexagon h in Hexagons) {
			h.IsSelected = false;
			h.IsHovering = false;
		}
	}


	public bool IsPreviousUnlocked(Hexagon hexagon) {
		int i = Hexagons.IndexOf(hexagon);

		if(i == 0)
			return true;

		return Hexagons[i - 1].IsUnlocked;
	}


	private void Update() {
		if(Input.GetMouseButtonDown(0) && (!UICamera.isOverUI || UICamera.hoveredObject.GetComponent<UIButton>() == null)) {
			UpgradeMenu.ins.DeselectHexagons();
		}

		isSelected = false;

		foreach(Hexagon h in Hexagons) {
			if(h.IsHovering || h.IsSelected)
				isSelected = true;
		}

		targetPos = isSelected ? SelectedPos : startPos;

		transform.localPosition = Vector3.Lerp(transform.localPosition, targetPos, Time.unscaledDeltaTime * 6f);
		widget.alpha = Mathf.Lerp(widget.alpha, targetAlpha, Time.unscaledDeltaTime * 2f);
		transform.localScale = Vector3.Lerp(transform.localScale, startScale * targetScale, Time.unscaledDeltaTime * 8f);
	}
}
