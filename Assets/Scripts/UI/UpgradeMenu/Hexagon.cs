﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hexagon : MonoBehaviour {
	public Color UnlockedColor;
	public Color InfoFontColor;
	public Color AvailableColor;
	public string UpgradeID;
	[HideInInspector]
	public PowerUpHexagon hexagonGroup;

	public bool IsHovering {
		get { return isHovering; }
		set { isHovering = value; }
	}
	public bool IsSelected {
		get { return isSelected; }
		set { isSelected = value; }
	}
	public bool IsUnlocked {
		get { return isUnlocked; }
	}

	private bool isUnlocked = false;
	private bool isHovering = false;
	private bool isSelected = false;

	private Vector3 startScale;
	private Vector3 hoverScale;
	private Vector3 newScale;

	private UISprite sprite;
	private UIButton button;


	private void Awake() {
		hexagonGroup = transform.parent.GetComponent<PowerUpHexagon>();

		if(hexagonGroup == null) {
			Debug.LogError("No PowerUp group available!", transform);
			return;
		}

		sprite = GetComponent<UISprite>();
		button = GetComponent<UIButton>();

		startScale = transform.localScale;
		hoverScale = startScale + Vector3.one * 0.1f;

		UIEventListener.Get (gameObject).onHover += delegate(GameObject go, bool state) {
			isHovering = state;

			if(isHovering) {
				UpgradeMenu.ins.DisplayInfo(this, InfoFontColor);
			} else {
				UpgradeMenu.ins.DisplayInfo(null, Color.white);
			}
		};

		UIEventListener.Get (gameObject).onSelect += delegate(GameObject go, bool state) {
			if(!state && (!UICamera.isOverUI || UICamera.hoveredObject.name == "[Button] Upgrade"))
				return;

			UpgradeMenu.ins.DeselectHexagons();

			isSelected = state;

			if(isSelected) {
				UpgradeMenu.ins.DisplayInfo(this, InfoFontColor, true);
			} else {
				UpgradeMenu.ins.DisplayInfo(null, Color.white);
			}
		};
	}


	public bool IsAvailable() {
		return hexagonGroup.IsPreviousUnlocked(this);
	}


	public void Unlock() {
		isUnlocked = true;
		sprite.spriteName = "hexagonShape";
		button.normalSprite = sprite.spriteName;
		sprite.color = UnlockedColor;
		button.defaultColor = UnlockedColor;
		button.disabledColor = UnlockedColor;
		button.hover = UnlockedColor;
		button.pressed = UnlockedColor;
	}


	private void Update() {
		if(!isUnlocked && IsAvailable()) {
			sprite.spriteName = "hexagonShapeAvailable";
			button.normalSprite = sprite.spriteName;
			sprite.color = AvailableColor;
			button.defaultColor = AvailableColor;
		}

		newScale = Vector3.Lerp(newScale, (isHovering || isSelected) ? hoverScale : startScale, Time.unscaledDeltaTime * 6f);

		transform.localScale = newScale;
	}
}
