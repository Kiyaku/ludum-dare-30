﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PanelManager : Singleton<PanelManager>
{
    public UIPanel CurrentPanel { get; protected set; }

    public UIPanel StartPanel;
    public UIPanel TransitionPanel;
	public Transform Cursor;

	public List<UIPanel> StaticPanels = new List<UIPanel>();

    private List<UIPanel> PanelList = new List<UIPanel>();
    private Vector3 nirvana = new Vector3(10000, 10000, 10000);
    private bool startPanel = true;
    private string lastPanel = "";
	private Blur blurEffect;
	private bool usedBlur = false;


    private void Awake()
    {
		blurEffect = Camera.main.GetComponent<Blur>();

        foreach (Transform child in transform)
        {
            UIPanel childPanel = child.GetComponent<UIPanel>();

            if (childPanel != null)
            {
                PanelList.Add(childPanel);
            }
        }
    }

    private void Start()
    {
       	if (StartPanel != null)
        {
            ShowPanel(StartPanel, true, false);
			Cursor.gameObject.SetActive(false);
        }
    }

    public UIPanel GetPanelByName(string name)
    {
        foreach (UIPanel panel in PanelList)
        {
            if (panel.name == name || panel.name == "[Panel] " + name)
            {
                return panel;
            }
        }

        return null;
    }

    public void ShowLastPanel()
    {
        ShowPanel(lastPanel);
    }

    public void SetLastPanel(UIPanel panel)
    {
        if(panel != null)
            SetLastPanel(panel.name);
    }

    public void SetLastPanel(string panelName)
    {
        lastPanel = panelName;
    }

	public void ShowPanel(UIPanel panel, bool disableOthers = true, bool blurBackground = true, bool showCursor = true)
    {
		ShowPanel(panel.name, disableOthers, blurBackground);
    }

	public void ShowPanel(string panelName, bool disableOthers = true, bool blurBackground = true, bool showCursor = true)
    {
		Cursor.gameObject.SetActive(showCursor);


		if(blurBackground && !usedBlur) {
			usedBlur = true;
			StartCoroutine(AnimateMenu(true));
		}

        if (disableOthers && CurrentPanel != null)
        {
            lastPanel = CurrentPanel.name;
        }

        foreach (UIPanel panel in PanelList)
        {
            if (panel.name == panelName || panel.name == "[Panel] " + panelName)
            {
                if (disableOthers)
                    CurrentPanel = panel;

                panel.transform.localPosition = Vector3.zero;
                panel.enabled = true;
                panel.SendMessage("ShowPanel", SendMessageOptions.DontRequireReceiver);
            }
            else if (disableOthers)
            {
                // Don't disable transition panel
				if (TransitionPanel != null && panel == TransitionPanel || StaticPanels.Contains(panel))
                {
                    continue;
                }

				panel.transform.localPosition = nirvana;
                panel.enabled = false;
                panel.SendMessage("HidePanel", SendMessageOptions.DontRequireReceiver);
            }
        }

        if (!startPanel && TransitionPanel != null && disableOthers)
        {
            TransitionPanel.SendMessage("ShowPanel", SendMessageOptions.DontRequireReceiver);
        }

        startPanel = false;
    }

    public void HidePanel(UIPanel panel)
    {
        HidePanel(panel.name);
    }

    public void HidePanel(string panelName)
    {
		bool somePanelActive = false;

		if(usedBlur) {
			usedBlur = false;
			StartCoroutine(AnimateMenu(false));
		}

        foreach (UIPanel panel in PanelList)
        {
            if (TransitionPanel != null && panel == TransitionPanel)
            {
                continue;
            }

            if (panel.name == panelName || panel.name == "[Panel] " + panelName)
            {
                panel.transform.position = nirvana;
                panel.enabled = false;
                panel.SendMessage("HidePanel", SendMessageOptions.DontRequireReceiver);
            } else if(panel.enabled && !StaticPanels.Contains(panel)) {
				somePanelActive = true;
			}
        }

		if(!somePanelActive) {
			ShowPanel(StartPanel, true, false);
		}

		Cursor.gameObject.SetActive(false);
    }


	private IEnumerator AnimateMenu(bool fadeIn) {
		float timer = 0;
		
		blurEffect.blurSize = fadeIn ? 0 : 4;
		blurEffect.enabled = true;

		while(timer < 1) {
			timer += Time.unscaledDeltaTime * 5f;
			
			if(fadeIn) {
				blurEffect.blurSize = timer * 4;
				blurEffect.blurIterations = (int)(timer * 3);
			} else {
				blurEffect.blurSize = (1f - timer) * 4;
				blurEffect.blurIterations = (int)((1f - timer) * 3);
			}
			
			yield return null;
		}
		
		if(fadeIn) {
			blurEffect.blurSize = 4;
		} else {
			blurEffect.blurSize = 0;
			blurEffect.enabled = false;
		}
	}
}