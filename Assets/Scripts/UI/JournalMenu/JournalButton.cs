﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JournalButton : MonoBehaviour {
	public UILabel TitleLabel;
	public UILabel TypeLabel;
	public UISprite BackgroundSprite;

	public Color NormalColor;
	public Color HoverColor;

	[HideInInspector]
	public string JournalID;

	private string normalSprite = "JournalEntryButton_Normal";
	private string hoverSprite = "JournalEntryButton_Hover";
	private string selectedSprite = "JournalEntryButton_Selected";

	private UIButton button;
	private bool isSelected = false;


	private void Awake() {
		button = GetComponent<UIButton>();

		UIEventListener.Get(gameObject).onSelect += delegate(GameObject go, bool state) {
			if(state) {
				Select ();
			}
		};

		UIEventListener.Get(gameObject).onHover = delegate(GameObject go, bool state) {
			if(!isSelected) {
				TitleLabel.color = state ? HoverColor : NormalColor;
			}
		};
	}


	public void SetInfo(string journalID) {
		JournalID = journalID;
		Journal j = JournalDB.ins.GetJournalByID(journalID);

		TitleLabel.text = j.Title;
		TypeLabel.text = j.Type == Journal.Types.MEMO ? "Journal Entry" : "Mission Info";
	}


	public void Select() {
		button.normalSprite = selectedSprite;
		button.hoverSprite = selectedSprite;
		TitleLabel.color = HoverColor;
		isSelected = true;
	}


	public void Deselect() {
		button.normalSprite = normalSprite;
		button.hoverSprite = hoverSprite;
		TitleLabel.color = NormalColor;
		isSelected = false;
	}
}
