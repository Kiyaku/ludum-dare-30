﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class IngameJournalUI : MonoBehaviour {
	public static IngameJournalUI ins;


	public UILabel LabelTitle;
	public UILabel LabelText;


	private void Awake() {
		ins = this;

		transform.localPosition = Vector3.zero;
	}


	public void OpenJournal(Journal j) {
		LabelTitle.text = j.Title;
		LabelText.text = j.Text;
		PlayerControl.ins.ToggleMouseCursor(true);

		PanelManager.Ins.ShowPanel(transform.name);
	}


	public void CloseJournal() {
		PanelManager.Ins.HidePanel(transform.name);

		if(PlayerControl.ins != null)
			PlayerControl.ins.ToggleMouseCursor(false);
	}
}
