﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JournalMenu : SubMenu {
	public static JournalMenu ins;

	public UILabel JournalTitle;
	public UILabel JournalText;
	public JournalButton ButtonPrefab;
	public UITable JournalList;
	public UIScrollView ScrollList;
	public UIButton MemoButton;
	public UIButton TaskButton;

	private List<JournalButton> buttonList = new List<JournalButton>();


	private void Awake() {
		ins = this;

		UIEventListener.Get (MemoButton.gameObject).onSelect = delegate(GameObject go, bool state) {
			if(state) {
				RefreshList(Journal.Types.MEMO);
			}
		};

		UIEventListener.Get (TaskButton.gameObject).onSelect = delegate(GameObject go, bool state) {
			if(state) {
				RefreshList(Journal.Types.TASK);
			}
		};
	}


	public override void ShowPanel(bool animate = false, int dir = 1) {
		base.ShowPanel(animate, dir);

		RefreshList(Journal.Types.MEMO);
	}


	private void RefreshList(Journal.Types type) {
		int index = 0;

		ScrollList.ResetPosition();
		
		JournalTitle.text = "";
		JournalText.text = "";

		foreach(JournalButton button in buttonList) {
			button.Deselect();
			button.gameObject.SetActive(false);
		}
		
		foreach(string journalID in JournalDB.ins.FoundJournalIDs) {
			Journal journal = JournalDB.ins.GetJournalByID(journalID);

			if(journal.Type == type) {
				JournalButton button;

				if(buttonList.Count > index) {
					button = buttonList[index];
					button.gameObject.SetActive(true);
				} else {
					button = NGUITools.AddChild(JournalList.gameObject, ButtonPrefab.gameObject).GetComponent<JournalButton>();
					buttonList.Add(button);

					UIEventListener.Get (button.gameObject).onSelect += delegate(GameObject go, bool state) {
						if(state)
							DisplayJournalEntry(button.JournalID);
					};
				}

				button.JournalID = journalID;
				button.SetInfo(journalID);
			}

			index++;
		}
		
		JournalList.GetComponent<UITable>().repositionNow = true;
	}


	public void DisplayJournalEntry(string journalID) {
		Journal journal = JournalDB.ins.GetJournalByID(journalID);
		JournalTitle.text = journal.Title;
		JournalText.text = journal.Text;

		foreach(JournalButton button in buttonList) {
			if(button.JournalID != journalID) {
				button.Deselect();
			}
		}
	}
}
