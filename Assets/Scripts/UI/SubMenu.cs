﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SubMenu : MonoBehaviour {
	public MainMenu.MenuTypes Type;

	public virtual void ShowPanel(bool animate = false, int dir = 1) {
		StopAllCoroutines();

		if(animate) {
			StartCoroutine(AnimateSubMenu(dir, true));
		} else {
			transform.localPosition = Vector3.zero;
		}
	}


	public virtual void HidePanel(bool animate = false, int dir = 1) {
		StopAllCoroutines();

		if(animate) {
			StartCoroutine(AnimateSubMenu(dir, false));
		} else {
			transform.localPosition = Helper.NIRVANA;
		}
	}


	private IEnumerator AnimateSubMenu(int dir, bool show) {
		float timer = 0;
		Vector3 startPos;
		Vector3 endPos;

		if(show) {
			transform.localPosition = new Vector3(-dir * 2000, 0, 0);
			endPos = Vector3.zero;
		} else {
			endPos = new Vector3(dir * 2000, 0, 0);
		}

		startPos = transform.localPosition;

		while(timer < 1) {
			transform.localPosition = Vector3.Lerp(transform.localPosition, endPos, timer);

			timer += Time.unscaledDeltaTime;

			yield return null;
		}

		if(show) {
			transform.localPosition = Vector3.zero;
		} else {
			transform.localPosition = Helper.NIRVANA;
		}
	}
}
