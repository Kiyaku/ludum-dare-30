﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(UISlider))]
public class ToggleSlider : MonoBehaviour 
{
    public bool value { get { return _uiSlider.value >= 1f; } set { _uiSlider.value = value ? 1f : 0f; DoUpdate(); _uiSlider.ForceUpdate(); } }

    public bool clampThumb;
    public float minThumb;
    public float maxThumb;

    public List<EventDelegate> onChange = new List<EventDelegate>();

    [SerializeField]
    private UISlider _uiSlider;


    void Awake() {
        _uiSlider.onChange.Add(new EventDelegate(new EventDelegate.Callback(DoUpdate)));
    }


	private void Update() {
		DoUpdate();
	}


    public void DoToggle() {
        DoSliderValueClamp();

        if (_uiSlider.value >= 1f) {
            _uiSlider.value = 0f;
        }
        else {
            _uiSlider.value = 1f;
        }

        DoUpdate();

        _uiSlider.ForceUpdate();
    }


    private void DoUpdate() {
        DoSliderValueClamp();

        if (clampThumb) {
        	if (_uiSlider.thumb != null) {
                _uiSlider.thumb.localPosition = new Vector3(
                    _uiSlider.fillDirection == UIProgressBar.FillDirection.LeftToRight || _uiSlider.fillDirection == UIProgressBar.FillDirection.RightToLeft ? Mathf.Clamp(_uiSlider.thumb.localPosition.x, minThumb, maxThumb) : _uiSlider.thumb.transform.localPosition.x,
                    _uiSlider.fillDirection == UIProgressBar.FillDirection.BottomToTop || _uiSlider.fillDirection == UIProgressBar.FillDirection.TopToBottom ? Mathf.Clamp(_uiSlider.thumb.localPosition.y, minThumb, maxThumb) : _uiSlider.thumb.transform.localPosition.y,
                    _uiSlider.thumb.transform.localPosition.z);
            }
        }

        for (int i = 0; i < onChange.Count; ++i) {
            if (onChange[i] != null)
                onChange[i].Execute();
        }
    }


    private void DoSliderValueClamp() {
        _uiSlider.value = Mathf.Round(_uiSlider.value);
    }


    void Reset() {
        _uiSlider = GetComponent<UISlider>();
    }
}