using UnityEngine;

public class Cursor : MonoBehaviour
{
	static public Cursor instance;
	public Camera uiCamera;

	Transform mTrans;

	void Awake () { instance = this; }
	void OnDestroy () { instance = null; }

	/// <summary>
	/// Cache the expected components and starting values.
	/// </summary>

	void Start ()
	{
		mTrans = transform;
		GetComponent<UIPanel>().depth = 100;
	}

	/// <summary>
	/// Reposition the widget.
	/// </summary>

	void Update ()
	{
		Vector3 pos = Input.mousePosition;

		if (uiCamera != null)
		{
			// Since the screen can be of different than expected size, we want to convert
			// mouse coordinates to view space, then convert that to world position.
			pos.x = Mathf.Clamp01(pos.x / Screen.width);
			pos.y = Mathf.Clamp01(pos.y / Screen.height);
			mTrans.position = uiCamera.ViewportToWorldPoint(pos);

			// For pixel-perfect results
			if (uiCamera.isOrthoGraphic)
			{
				Vector3 lp = mTrans.localPosition;
				lp.x = Mathf.Round(lp.x);
				lp.y = Mathf.Round(lp.y);
				mTrans.localPosition = lp;
			}
		}
		else
		{
			// Simple calculation that assumes that the camera is of fixed size
			pos.x -= Screen.width * 0.5f;
			pos.y -= Screen.height * 0.5f;
			pos.x = Mathf.Round(pos.x);
			pos.y = Mathf.Round(pos.y);
			mTrans.localPosition = pos;
		}
	}
}
