﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ComLink : MonoBehaviour {
	public static ComLink ins;

	public UITexture WaveformTexture;
	public UITexture ProfilePicture;
	public UITexture NoiseImage;
	public Color WaveformColor;

	public Texture2D[] NoiseTextures;

	private int waveformWidth;
	private int waveformHeight;
	private Color[] colorBlank;
	private Color colorBG = new Color(0, 0, 0, 0);
	private Texture2D waveformTexture;
	private float[] audioSamples;
	private int waveLength = 512;
	private Vector3 startPos;
	private UIPanel panel;


	private void Awake() {
		ins = this;
		panel = GetComponent<UIPanel>();

		panel.alpha = 0;

		startPos = transform.localPosition;

		waveformWidth = WaveformTexture.width;
		waveformHeight = WaveformTexture.height;

		audioSamples = new float[waveLength];

		waveformTexture = new Texture2D(waveformWidth, waveformHeight, TextureFormat.RGBA32, false);
		WaveformTexture.mainTexture = waveformTexture;

		colorBlank = new Color[waveformWidth * waveformHeight];

		// set blank color to reset waveform texture
		for(int i = 0; i < colorBlank.Length; i++) {
			colorBlank[i] = colorBG;
		}

		DrawWaveform();
	}


	private IEnumerator Start() {
		yield return new WaitForSeconds(5);

		//PlayMessage(null, null);
	}


	public void PlayMessage(AudioClip clip, Texture2D profilePicture) {
		//audio.clip = clip;

		//ProfilePicture.mainTexture = profilePicture;
		NoiseImage.enabled = false;
		ProfilePicture.enabled = false;

		transform.localPosition = startPos;

		StartCoroutine(UpdateWaveForm());
	}


	private IEnumerator UpdateWaveForm() {
		float timer = 0;

		while(timer < 1) {
			timer += Time.deltaTime * 3f;
			panel.alpha = timer;

			yield return null;
		}

		timer = 1;
		panel.alpha = timer;

		audio.Play();
		NoiseImage.enabled = true;
		ProfilePicture.enabled = true;

		while(audio.isPlaying) {
			DrawWaveform();
			NoiseImage.mainTexture = NoiseTextures[Random.Range(0, NoiseTextures.Length)];
			NoiseImage.alpha = Random.Range(0.2f, 0.4f);

			yield return new WaitForSeconds(0.05f);
		}
		
		NoiseImage.enabled = false;
		ProfilePicture.enabled = false;

		while(timer > 0) {
			timer -= Time.deltaTime * 3f;
			panel.alpha = timer;
			
			yield return null;
		}

		panel.alpha = 0;

		transform.localPosition = Helper.NIRVANA;
	}


	private void DrawWaveform() {
		waveformTexture.SetPixels(colorBlank);

		audio.GetOutputData(audioSamples, 0);

		if(audio.isPlaying) {
			for(int i = 0; i < waveLength; i++) {
				waveformTexture.SetPixel((int)((waveformWidth * i) / waveLength), (int)(waveformHeight * (2 * audioSamples[i] + 1f) / 2f), WaveformColor);
			}
		}

		waveformTexture.Apply();
	}
}
