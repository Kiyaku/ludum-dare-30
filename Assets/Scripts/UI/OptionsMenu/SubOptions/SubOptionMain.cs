﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SubOptionMain : SubOptionsMenu {
	public UIButton SettingsButton;
	
	
	private void Awake() {
		UIEventListener.Get(SettingsButton.gameObject).onClick = delegate(GameObject go) {
			OptionsMenu.ins.ShowOptionsMenu(OptionsMenu.SubOptionsTypes.SETTINGS);
		};
	}
}
