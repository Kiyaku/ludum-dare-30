﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SubOptionControls : SubOptionsMenu {
	public UISlider SliderMouseX;
	public UISlider SliderMouseY;
	public UILabel LabelMouseX;
	public UILabel LabelMouseY;
	public UIButton BackButton;
	
	
	private void Awake() {
		EventDelegate.Add(SliderMouseX.onChange, OnChangeMouseX);
		EventDelegate.Add(SliderMouseY.onChange, OnChangeMouseY);

		SliderMouseX.value = ((float)SettingsManager.MouseSensitivityX / (float)(SettingsManager.MaxMouseSensitivity - SettingsManager.MinMouseSensitivity));
		SliderMouseY.value = ((float)SettingsManager.MouseSensitivityY / (float)(SettingsManager.MaxMouseSensitivity - SettingsManager.MinMouseSensitivity));

		UIEventListener.Get(BackButton.gameObject).onClick = delegate(GameObject go) {
			OptionsMenu.ins.ShowOptionsMenu(OptionsMenu.SubOptionsTypes.SETTINGS);
		};
	}


	private void OnChangeMouseX() {
		SettingsManager.SetMouseSensitivityX(SliderMouseX.value);
		LabelMouseX.text = SettingsManager.MouseSensitivityX.ToString();
	}


	private void OnChangeMouseY() {
		SettingsManager.SetMouseSensitivityY(SliderMouseY.value);
		LabelMouseY.text = SettingsManager.MouseSensitivityY.ToString();
	}
}
