﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SubOptionAudio : SubOptionsMenu {
	public UIButton BackButton;
	
	
	private void Awake() {
		UIEventListener.Get(BackButton.gameObject).onClick = delegate(GameObject go) {
			OptionsMenu.ins.ShowOptionsMenu(OptionsMenu.SubOptionsTypes.SETTINGS);
		};
	}
}
