﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SubOptionGame : SubOptionsMenu {
	public UISlider SliderHeadBobbing;
	public UIButton BackButton;


	private void Awake() {
		EventDelegate.Add (SliderHeadBobbing.onChange, OnChangeHeadBobbing);

		SliderHeadBobbing.value = SettingsManager.UseHeadBobbing ? 1 : 0;

		UIEventListener.Get(BackButton.gameObject).onClick = delegate(GameObject go) {
			OptionsMenu.ins.ShowOptionsMenu(OptionsMenu.SubOptionsTypes.SETTINGS);
		};
	}


	private void OnChangeHeadBobbing() {
		SettingsManager.UseHeadBobbing = (int)SliderHeadBobbing.value == 1;
	}
}
