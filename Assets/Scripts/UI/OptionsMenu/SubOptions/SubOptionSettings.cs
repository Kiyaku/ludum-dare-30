﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SubOptionSettings : SubOptionsMenu {
	public UIButton VideoButton;
	public UIButton AudioButton;
	public UIButton GameButton;
	public UIButton ControlsButton;
	public UIButton BackButton;


	private void Awake() {
		UIEventListener.Get(VideoButton.gameObject).onClick = delegate(GameObject go) {
			OptionsMenu.ins.ShowOptionsMenu(OptionsMenu.SubOptionsTypes.VIDEO);
		};

		UIEventListener.Get(AudioButton.gameObject).onClick = delegate(GameObject go) {
			OptionsMenu.ins.ShowOptionsMenu(OptionsMenu.SubOptionsTypes.AUDIO);
		};

		UIEventListener.Get(GameButton.gameObject).onClick = delegate(GameObject go) {
			OptionsMenu.ins.ShowOptionsMenu(OptionsMenu.SubOptionsTypes.GAME);
		};

		UIEventListener.Get(ControlsButton.gameObject).onClick = delegate(GameObject go) {
			OptionsMenu.ins.ShowOptionsMenu(OptionsMenu.SubOptionsTypes.CONTROLS);
		};

		UIEventListener.Get(BackButton.gameObject).onClick = delegate(GameObject go) {
			OptionsMenu.ins.ShowOptionsMenu(OptionsMenu.SubOptionsTypes.MAIN);
		};
	}
}
