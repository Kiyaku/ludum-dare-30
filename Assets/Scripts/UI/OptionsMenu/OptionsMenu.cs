﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OptionsMenu : SubMenu {
	public static OptionsMenu ins;

	public enum SubOptionsTypes {
		MAIN,
		SETTINGS,
		VIDEO,
		AUDIO,
		GAME,
		CONTROLS
	}

	public UILabel OptionTitle;
	public SubOptionsTypes DefaultMenu;
	public List<SubOptionsMenu> Menus = new List<SubOptionsMenu>();

	private SubOptionsMenu currentMenu;


	private void Awake() {
		ins = this;
	}


	public override void ShowPanel (bool animate, int dir) {
		base.ShowPanel (animate, dir);

		foreach(SubOptionsMenu som in Menus)
			som.HideMenu(true);

		ShowOptionsMenu(DefaultMenu, true);
	}


	public void ShowOptionsMenu(SubOptionsTypes type, bool instant = false) {
		SubOptionsMenu nextMenu = Menus.Find(m => m.Type == type);

		if(nextMenu == null)
			return;

		OptionTitle.text = nextMenu.Title;

		if(currentMenu != null) {
			currentMenu.HideMenu(instant);
		}

		currentMenu = nextMenu;

		currentMenu.ShowMenu(instant);
	}
}
