﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SubOptionsMenu : MonoBehaviour {
	public string Title;
	public OptionsMenu.SubOptionsTypes Type;
	public List<Transform> ButtonList = new List<Transform>();

	private float globalDelay = 0.075f;
	private Transform lastTarget;


	public void ShowMenu(bool instant = false) {
		float index = 0;

		CancelInvoke("DisableMenu");
		gameObject.SetActive(true);

		StopAllCoroutines();

		foreach(Transform button in ButtonList) {
			if(instant) {
				button.localEulerAngles = new Vector3(0, 0, 0);
			} else {
				button.localEulerAngles = new Vector3(90, 0, 0);
				StartCoroutine(RotateButton(button, 0, globalDelay * index + 0.2f));

				index++;
			}
		}
	}


	public void HideMenu(bool instant = false) {
		float index = 0;

		StopAllCoroutines();

		foreach(Transform button in ButtonList) {
			if(instant) {
				button.localEulerAngles = new Vector3(90, 0, 0);
			} else {
				button.localEulerAngles = new Vector3(0, 0, 0);
				StartCoroutine(RotateButton(button, 90, globalDelay * index, true));
				
				index++;
			}
		}
	}


	private IEnumerator RotateButton(Transform target, float targetRot, float delay, bool hide = false) {
		float timer = 0;
		Vector3 startRot = target.localEulerAngles;
		Vector3 endRot = new Vector3(targetRot, 0, 0);

		lastTarget = target;

		// Can't use WaitForSeconds because of timescale
		while(timer < delay) {
			timer += Time.unscaledDeltaTime;
			yield return null;
		}

		timer = 0;

		while(timer < 1) {
			target.localEulerAngles = Vector3.Lerp(startRot, endRot, timer);

			timer += Time.unscaledDeltaTime * 4f;

			yield return null;
		}

		if(lastTarget == target && hide) {
			gameObject.SetActive(false);
		}

		target.localEulerAngles = endRot;
	}
}
