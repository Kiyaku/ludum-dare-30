﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class InventoryMenu : SubMenu {
	public UIGrid InventoryGrid;
	public UIGrid ButtonGrid;
	public GameObject InventoryIcon;
	public ItemButton ButtonPrefab;
	public UILabel ItemName;
	public UILabel ItemDesc;
	public UIButton ButtonEquip;
	public UIButton ButtonUse;
	public UIButton ButtonDrop;

	private List<ItemButton> itemButtons = new List<ItemButton>();
	private bool isOpen = false;
	private ItemButton selectedItem;
	private Inventory currentInventory;


	private void Awake() {
		ItemName.enabled = false;
		ItemDesc.enabled = false;

		UIEventListener.Get (ButtonDrop.gameObject).onClick += delegate(GameObject go) {
			DropItem();
		};
		
		for(int i = 0; i < 25; i++) {
			GameObject child = Instantiate (InventoryIcon) as GameObject;
			InventoryGrid.AddChild(child.transform);
			child.transform.localScale = Vector3.one;
		}

		CloseInventory();
	}


	public bool ToggleInventory(Inventory inv) {
		if(isOpen)
			CloseInventory();
		else
			OpenInventory(inv);

		return isOpen;
	}


	public bool CloseInventory() {
		isOpen = false;
		selectedItem = null;

		if(PlayerControl.ins != null)
			PlayerControl.ins.ToggleMouseCursor(false);

		return false;
	}


	private void ShowItemInfo(Item item) {
		ItemName.text = item.Name;
		ItemDesc.text = item.Description;
		ItemName.enabled = true;
		ItemDesc.enabled = true;
	}


	private void HideItemInfo() {
		if(selectedItem != null) {
			ShowItemInfo(selectedItem.AssignedItem);
		} else {
			ItemName.enabled = false;
			ItemDesc.enabled = false;
		}
	}


	private void DropItem() {
		if(selectedItem == null || !currentInventory.HasItem(selectedItem.AssignedItem.ID))
		   return;

		Vector3 dropPos = PlayerControl.ins.CameraPtr.position;
		dropPos += PlayerControl.ins.CameraPtr.forward;

		Transform worldItem = Instantiate(selectedItem.AssignedItem.WorldPrefab, dropPos, Quaternion.identity) as Transform;

		currentInventory.RemoveItem(selectedItem.AssignedItem.ID, 1);

		if(!currentInventory.HasItem(selectedItem.AssignedItem.ID)) {
			selectedItem = null;
			HideItemInfo();
		}

		OpenInventory(currentInventory);
	}


	private void Update() {
		if(isOpen) {
			if(selectedItem != null) {
				ButtonDrop.isEnabled = true;
			} else {
				ButtonDrop.isEnabled = false;
			}
		}
	}


	public void OpenInventory(Inventory inv) {
		isOpen = true;
		currentInventory = inv;
		PlayerControl.ins.ToggleMouseCursor(true);

		foreach(ItemButton button in itemButtons) {
			button.gameObject.SetActive(false);
		}

		for(int i = 0; i < inv.Items.Count; i++) {
			Item item = ItemDB.ins.GetItemByID(inv.Items[i].ItemID);
			ItemButton button;

			if(itemButtons.Count > i) {
				button = itemButtons[i];
				button.gameObject.SetActive(true);
			} else {
				ItemButton child = Instantiate (ButtonPrefab) as ItemButton;
				ButtonGrid.AddChild(child.transform);
				child.transform.localScale = Vector3.one;
				button = child;

				itemButtons.Add(button);

				UIEventListener.Get (button.gameObject).onHover += delegate(GameObject go, bool state) {
					if(state)
						ShowItemInfo(button.AssignedItem);
					else
						HideItemInfo();
				};

				UIEventListener.Get (button.gameObject).onClick += delegate(GameObject go) {
					selectedItem = button;
				};
			}

			button.SetupButton(item, inv.Items[i].Quantity);
		}
	}
}
