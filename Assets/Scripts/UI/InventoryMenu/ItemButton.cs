﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ItemButton : MonoBehaviour {
	[HideInInspector]
	public Item AssignedItem;
	public UILabel QuantityLabel;

	private UITexture itemTexture;


	private void Awake() {
		itemTexture = GetComponent<UITexture>();
	}


	public void SetupButton(Item item, int quantity) {
		if(item != null) {
			AssignedItem = item;
			itemTexture.mainTexture = item.Image;
			QuantityLabel.text = "x" + quantity;
		}
	}
}
