﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class Receiver : MonoBehaviour {
	public Activator Trigger;

	public string MethodOnActivate;
	public string MethodOnDeactivate;


	private void Awake() {
		if(Trigger != null) {
			Trigger.OnActivated += OnActivate;
			Trigger.OnDeactivated += OnDeactivate;
		}
	}


	private void OnDisable() {
		if(Trigger != null) {
			Trigger.OnActivated -= OnActivate;
			Trigger.OnDeactivated -= OnDeactivate;
		}
	}


	private void OnActivate() {
		transform.SendMessage(MethodOnActivate, SendMessageOptions.DontRequireReceiver);
	}


	private void OnDeactivate() {
		transform.SendMessage(MethodOnDeactivate, SendMessageOptions.DontRequireReceiver);
	}
}
