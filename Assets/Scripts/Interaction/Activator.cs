﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Activator : MonoBehaviour {
	public delegate void StatusUpdate();
	public event StatusUpdate OnActivated;
	public event StatusUpdate OnDeactivated;

	public bool IsActive = false;


	public virtual void Activate() {
		if(OnActivated != null) {
			IsActive = true;
			OnActivated();
		}
	}


	public virtual void Deactivate() {
		if(OnDeactivated != null) {
			IsActive = false;
			OnDeactivated();
		}
	}
}
