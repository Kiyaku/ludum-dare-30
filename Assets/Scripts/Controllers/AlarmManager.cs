﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class AlarmManager : MonoBehaviour {
	public static AlarmManager ins;
	[HideInInspector]
	public Vector3 LastKnownPlayerPos;

	public Light AlarmSpotlight;

	private bool inAlarmState = false;
	private float lightIntensity;
	private float alarmCoolDown = 0;
	private float maxAlarmTime = 10;


	public delegate void TriggeredAlarm(Vector3 pos, Transform triggerer);
	public static event TriggeredAlarm OnTriggeredAlarm;


	private void Awake() {
		ins = this;
	}


	public void TriggerAlarm(Vector3 lastKnownPos, Transform triggerer) {
		if(inAlarmState)
			return;

		alarmCoolDown = 0;
		inAlarmState = true;
		LastKnownPlayerPos = lastKnownPos;
		AlarmSpotlight.enabled = true;

		if(OnTriggeredAlarm != null)
			OnTriggeredAlarm(LastKnownPlayerPos, triggerer);
	}


	public void ResetAlarmTimer() {
		alarmCoolDown = 0;
	}


	private void Update() {
		if(inAlarmState) {
			AlarmSpotlight.intensity = Mathf.Abs (Mathf.Sin (lightIntensity * Mathf.Deg2Rad)) + 0.5f;
			lightIntensity += 100f * Time.deltaTime;

			if(lightIntensity > 360)
				lightIntensity -= 360;

			alarmCoolDown += Time.deltaTime;

			if(alarmCoolDown >= maxAlarmTime) {
				inAlarmState = false;
				AlarmSpotlight.enabled = false;
				alarmCoolDown = 0;
			}
		}
	}
}
