﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Inventory : MonoBehaviour {
	[System.Serializable]
	public class OwnedItems {
		public string ItemID;
		public int Quantity;

		public OwnedItems(string id, int quantity) {
			ItemID = id;
			Quantity = quantity;
		}
	}

	public List<OwnedItems> Items = new List<OwnedItems>();
	public List<string> Journals = new List<string>();

	private int maxInventorySize = 25;


	public bool HasItem(Item item) {
		return HasItem (item.ID);
	}


	public bool HasItem(string itemID) {
		return Items.Find(i => i.ItemID == itemID && i.Quantity > 0) != null;
	}


	public void RemoveItem(string itemID, int quantity) {
		OwnedItems item = Items.Find(i => i.ItemID == itemID);

		if(item != null) {
			item.Quantity -= quantity;

			if(item.Quantity <= 0) {
				Items.Remove(item);
			}
		}
	}


	public bool AddItem(string itemID, int quantity) {
		OwnedItems item = Items.Find(i => i.ItemID == itemID);

		if(item != null) {
			item.Quantity += quantity;

			return true;
		} else if(Items.Count >= 25) {
			return false;
		} else {
			item = new OwnedItems(itemID, quantity);
			Items.Add(item);

			return true;
		}
	}


	public void AddJournal(string journalID) {
		if(!Journals.Contains(journalID)) {
			Journals.Add(journalID);
			Debug.Log ("Journal added");
		}
	}
}
