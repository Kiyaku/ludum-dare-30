﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class EnemyDetector : MonoBehaviour {
	public EnemyControl enemyControl;


	private void OnTriggerStay(Collider col) {
		if(col.tag == "Player") {
			if(enemyControl.currentState != EnemyControl.State.CHASING && enemyControl.currentState != EnemyControl.State.ATTACKING && enemyControl.currentState != EnemyControl.State.SEARCHING) {
				if(enemyControl.CanSeePlayer()) {
					enemyControl.currentState = EnemyControl.State.SUSPICIOUS;
				}
			}
		}
	}
}
