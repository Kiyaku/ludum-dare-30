﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class SettingsManager {
	static SettingsManager() {
		// Input Default
		if(PlayerPrefs.HasKey(ppMouseSensitivityX))
			mouseSensitivityX = PlayerPrefs.GetInt(ppMouseSensitivityX);
		
		if(PlayerPrefs.HasKey(ppMouseSensitivityY))
			mouseSensitivityY = PlayerPrefs.GetInt(ppMouseSensitivityY);

		// Game Options Default
		useHeadBobbing = PlayerPrefs.GetInt(ppUseHeadBobbing, 1) == 1;
	}


	#region Input
	public static int MinMouseSensitivity = 1;
	public static int MaxMouseSensitivity = 100;

	public static int MouseSensitivityX  {
		get {
			return mouseSensitivityX; 
		}
	}
	public static int MouseSensitivityY  {
		get {
			return mouseSensitivityY; 
		}
	}


	private static int mouseSensitivityX = 50;
	private static int mouseSensitivityY = 50;

	private static string ppMouseSensitivityX = "MouseSensitivityX";
	private static string ppMouseSensitivityY = "MouseSensitivityY";


	public static void SetMouseSensitivityX(float percentage) {
		mouseSensitivityX = (int)Mathf.Lerp(MinMouseSensitivity, MaxMouseSensitivity, percentage);
		PlayerPrefs.SetInt(ppMouseSensitivityX, mouseSensitivityX);
	}

	public static void SetMouseSensitivityY(float percentage) {
		mouseSensitivityY = (int)Mathf.Lerp(MinMouseSensitivity, MaxMouseSensitivity, percentage);
		PlayerPrefs.SetInt(ppMouseSensitivityY, mouseSensitivityY);
	}
	#endregion

	#region GameOptions
	private static bool useHeadBobbing = true;
	public static bool UseHeadBobbing {
		get {
			return useHeadBobbing;
		}
		set {
			useHeadBobbing = value;
			PlayerPrefs.SetInt(ppUseHeadBobbing, value ? 1 : 0);
		}
	}

	private static string ppUseHeadBobbing = "UseHeadBobbing";
	#endregion
}
