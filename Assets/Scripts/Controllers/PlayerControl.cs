﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerControl : MonoBehaviour {
	public static PlayerControl ins;

	private enum MovementState {
		MOVING,
		CLIMBING
	}

	public float MovementSpeed = 5;
	public float CameraSpeed = 20;
	public float Gravity = 10;
	public float JumpForce = 20;
	public float SprintModifier = 1.5f;
	public float LadderSpeedModifier = 0.3f;
	public float Stamina = 100;
	public UIProgressBar StaminaBar;
	public Transform CameraPtr;
	public InventoryMenu MenuInventory;
	public Inventory PlayerInventory;
	public Transform LaserBeamPrefab;
	public Transform GunPoint;
	public LayerMask ShootLayerMask;
	public Health PlayerHealth;

	public AudioClip SFXFootStep;
	public AudioClip SFXMetalCrate;

	private float hInput;
	private float vInput;
	private bool isCrouching = false;
	private bool isSprinting = false;
	private bool hasRoomToUnCrouch = true;
	private float gravityForce;
	private Vector3 movement;
	private CharacterController controller;

	private float crouchControllerHeight = 1.1f;
	private float controllerHeight;
	private int ladderCollider = 0;
	private float currentStamina;
	private float staminaCoolDown = 2;
	private float staminaTemp;
	private Vector3 defaultCamPos;
	private float crouchTimer = 0;
	private Highlighter highlighter;
	private bool canMove = true;
	private float viewBobTimer = 0;
	private float currentSpeedModifier;
	private Vector3 currentCamPos;

	private float footStepDelay = 0.3f;
	private float footStepTimer = 0;
	private bool isJumping = false;

	private bool isCarrying = false;
	private Transform carryingObject;

	private RaycastHit hit;
	private Ray ray;

	private MovementState currentMovementState = MovementState.MOVING;


	private void Awake() {
		ins = this;

		controller = GetComponent<CharacterController>();
		highlighter = GetComponent<Highlighter>();

		controllerHeight = controller.height;
		currentStamina = Stamina;
		defaultCamPos = CameraPtr.localPosition;
		currentCamPos = defaultCamPos;

		if(!Application.isEditor)
			CameraSpeed *= 7f;
	}


	private void Update() {
		if(Input.GetKeyDown (KeyCode.F12)) {
			string path = System.String.Format("screenshot_{0:yyyy_MM_dd_hh_mm_ss_tt}.png", System.DateTime.Now);
			Debug.Log ("Saved " + path);
			Application.CaptureScreenshot("..\\" + path, 2);
		}

		if(currentMovementState == MovementState.MOVING) {
			CheckSprinting();
			CheckCrouch();
		}

		if(Input.GetMouseButton(0) && canMove) {
			ToggleMouseCursor(false);
		}

		if(Input.GetMouseButtonDown(0) && canMove) {
			if(isCarrying) {
				DropObject(true);
			} else {
				Shoot();
			}
		}

		if(Input.GetKeyDown (KeyCode.J)) {
			if(canMove) {
				MainMenu.ins.OpenMenu(MainMenu.MenuTypes.JOURNAL);
				ToggleMouseCursor(true);
			}
		}
		
		if(Input.GetKeyDown (KeyCode.U)) {
			if(canMove) {
				MainMenu.ins.OpenMenu(MainMenu.MenuTypes.UPGRADE);
				ToggleMouseCursor(true);
			}
		}

		if(Input.GetKeyDown (KeyCode.I)) {
			if(canMove) {
				MainMenu.ins.OpenMenu(MainMenu.MenuTypes.INVENTORY);
				ToggleMouseCursor(true);
			}
		}

		if(Input.GetKeyDown (KeyCode.O)) {
			if(canMove) {
				MainMenu.ins.OpenMenu(MainMenu.MenuTypes.OPTIONS);
				ToggleMouseCursor(true);
			}
		}

		if(Input.GetKeyDown (KeyCode.Escape)) {
			if(!canMove) {
				MenuInventory.CloseInventory();
				IngameJournalUI.ins.CloseJournal();
				KeyCodePanel.ins.ClosePanel();
				MainMenu.ins.CloseMenu();
				ToggleMouseCursor(false);
			} else {
				MainMenu.ins.ToggleMenu();
				ToggleMouseCursor(true);
			}
		}

		if(Input.GetKeyDown(KeyCode.E) && canMove) {
			UseObject();
		}

		if(isCrouching) {
			ray = new Ray(transform.position - new Vector3(0, 0.3f, 0), Vector3.up);

			hasRoomToUnCrouch = true;

			if(Physics.Raycast(ray, out hit, 1f)) {
				hasRoomToUnCrouch = false;
			}
		}
	}


	private Vector3 ViewBobOffset() {
		if(!SettingsManager.UseHeadBobbing) {
			return Vector3.zero;
		}

		footStepTimer += Time.deltaTime * 700f + ((currentSpeedModifier - 1) * 5f);
		viewBobTimer += Time.deltaTime * 700f + ((currentSpeedModifier - 1) * 5f);

		float xOffset = (Mathf.Sin(viewBobTimer * Mathf.Deg2Rad) / 15f) * currentSpeedModifier;
		float yOffset = (Mathf.Sin(viewBobTimer * 2f * Mathf.Deg2Rad) / 22f) * currentSpeedModifier;

		if(viewBobTimer > 360) {
			viewBobTimer -= 360;
		}

		if(footStepTimer > 180) {
			footStepTimer = 0;
			audio.pitch = Random.Range(0.90f, 1f);
			audio.PlayOneShot(SFXFootStep);
		}

		Vector3 offset = Quaternion.Euler(new Vector3(0, transform.rotation.y, 0)) * new Vector3(xOffset, yOffset, 0);

		return offset;
	}


	public void CarryObject(Transform obj, float colSize) {
		carryingObject = obj;
		isCarrying = true;
		carryingObject.parent = transform;
		carryingObject.localPosition = new Vector3(0, 0.3f, colSize);
		carryingObject.localEulerAngles = new Vector3(0, 0, 0);

		StartCoroutine(AlignCarriedObject(0.3f, colSize));
	}


	// aligns the carried object infront of the player depending on the camera's rotation
	private IEnumerator AlignCarriedObject(float startOffset, float forwardOffset) {
		float yOffset = 0;
		float angleDifference = 80;
		float maxOffset = 1f;

		while(isCarrying) {
			// 360 = lowest | 360 - angleDifference = highest
			if(CameraPtr.localEulerAngles.x >= 360 - angleDifference) {
				yOffset = ((360 - CameraPtr.localEulerAngles.x) / angleDifference) * maxOffset;
			}

			if(carryingObject != null)
				carryingObject.localPosition = new Vector3(0, startOffset + yOffset, forwardOffset);

			yield return null;
		}
	}


	private void DropObject(bool isTossing) {
		if(carryingObject != null) {
			carryingObject.parent = null;
			carryingObject.SendMessage(isTossing ? "TossObject" : "DropObject", SendMessageOptions.DontRequireReceiver);
			carryingObject = null;
		}

		isCarrying = false;
	}


	private void Shoot() {
		if(currentStamina < 10) 
			return;

		Ray r = CameraPtr.camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
		RaycastHit h;

		Vector3 target = CameraPtr.camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 1000));

		if(Physics.Raycast(r, out h, 1000, ShootLayerMask)) {
			target = h.point;
		}

		Transform laser = Instantiate (LaserBeamPrefab, GunPoint.position, Quaternion.identity) as Transform;
		laser.rotation = Quaternion.LookRotation(target - GunPoint.position);

		currentStamina -= 10f;
		staminaTemp = staminaCoolDown / 4f;
	}


	public void ToggleMouseCursor(bool show) {
		Screen.showCursor = false;
		Screen.lockCursor = !show;
		canMove = !show;
	}


	private void UseObject() {
		if(isCarrying) {
			DropObject(false);
		} else if(highlighter.HighlightedObject != null) {
			highlighter.HighlightedObject.Trigger();
		}
	}


	private void FixedUpdate() {
		if(ladderCollider > 0) {
			currentMovementState = MovementState.CLIMBING;
		} else {
			currentMovementState = MovementState.MOVING;
		}
		
		MovePlayer();

		if(canMove) {
			MoveCamera();
		}
	}


	private void CheckSprinting() {
		isSprinting = Input.GetKey(KeyCode.LeftShift) && currentStamina > 0;

		StaminaBar.value = currentStamina / Stamina;

		if(isSprinting) {
			currentStamina -= 20f * Time.deltaTime;
		} else if(staminaTemp <= 0) {
			currentStamina += 30f * Time.deltaTime;
		}

		currentStamina = Mathf.Clamp(currentStamina, 0, Stamina);

		if(currentStamina <= 0 && staminaTemp <= 0) {
			staminaTemp = staminaCoolDown;
		}

		if(staminaTemp > 0)
			staminaTemp -= Time.deltaTime;
	}


	private void MovePlayer() {
		hInput = 0;
		vInput = 0;

		if(canMove) {
			hInput = Input.GetAxis("Horizontal");
			vInput = Input.GetAxis("Vertical");
		}

		Vector3 tempCamRot = new Vector3(0, CameraPtr.eulerAngles.y, CameraPtr.eulerAngles.z);
		currentSpeedModifier = (isCrouching ? 0.3f : 1f) * (isSprinting ? SprintModifier : 1);

		movement = Quaternion.Euler(tempCamRot) * new Vector3(hInput, 0, vInput);
		movement *= MovementSpeed * currentSpeedModifier;
	
		movement.y = 0;

		switch(currentMovementState) {
			case MovementState.MOVING:
			if(controller.isGrounded) {
				if(isJumping) {
					audio.PlayOneShot(SFXFootStep);
				}

				gravityForce = 0;
				isJumping = false;

				if(movement.magnitude > 0.1f) {
					CameraPtr.localPosition = currentCamPos + ViewBobOffset();
				} else {
					viewBobTimer = 0;
					footStepTimer = 0;
				}

				if(Input.GetAxis("Jump") > 0.2f) {
					isJumping = true;
					gravityForce = JumpForce * Time.deltaTime;
				}
			} else {
				gravityForce -= Gravity * Time.deltaTime;
			}

			movement.y = gravityForce;
			break;

			case MovementState.CLIMBING:
			movement.y = vInput * MovementSpeed * Mathf.Sign((CameraPtr.forward.y + 0.6f)) * (isSprinting ? SprintModifier : 1);
			movement *= LadderSpeedModifier;
			break;
		}

		controller.Move(movement * Time.deltaTime);
	}


	#region Crouching
	private void CheckCrouch() {
		if(!isCrouching && Input.GetKeyDown(KeyCode.C)) {
			Crouch();
		} 

		if(isCrouching && hasRoomToUnCrouch && !Input.GetKey(KeyCode.C)) {
			UnCrouch();
		}
	}


	private void Crouch() {
		isCrouching = true;
		controller.height -= crouchControllerHeight;
		controller.center -= new Vector3(0, crouchControllerHeight * 0.5f, 0);
		StartCoroutine(MoveCrouchCam(false));
	}


	private void UnCrouch() {
		isCrouching = false;
		controller.height += crouchControllerHeight;
		controller.center += new Vector3(0, crouchControllerHeight * 0.5f, 0);
		StartCoroutine(MoveCrouchCam(true));
	}


	private IEnumerator MoveCrouchCam(bool up) {
		crouchTimer = 1;

		yield return null;

		crouchTimer = 0;

		Vector3 oldPos = CameraPtr.localPosition;
		Vector3 newPos = up ? defaultCamPos : defaultCamPos - new Vector3(0, crouchControllerHeight, 0);

		while(crouchTimer < 1) {
			CameraPtr.localPosition = Vector3.Lerp(oldPos, newPos, crouchTimer);
			currentCamPos = CameraPtr.localPosition;
			crouchTimer += Time.deltaTime * 8f;

			yield return null;
		}

		CameraPtr.localPosition = newPos;
		currentCamPos = CameraPtr.localPosition;
	}
	#endregion


	private void MoveCamera() {
		Vector3 tempMovement = new Vector3(Input.GetAxis("Mouse Y") * SettingsManager.MouseSensitivityY, -Input.GetAxis("Mouse X") * SettingsManager.MouseSensitivityX, 0) * Time.deltaTime;
		Vector3 tempRot = CameraPtr.eulerAngles - tempMovement;

		Quaternion cameraRot = Quaternion.Euler(tempRot);

		float rotY = cameraRot.eulerAngles.y;
		float rotX = cameraRot.eulerAngles.x;

		rotY = Helper.ClampAngle(rotY, -360, 360);
		rotX = Helper.ClampAngle(rotX, -85, 85);
		rotX = -rotX;

		Quaternion quatX = Quaternion.AngleAxis(rotY, Vector3.up);
		Quaternion quatY = Quaternion.AngleAxis(rotX, Vector3.left);

		CameraPtr.localRotation = quatY;
		transform.rotation = quatX;
	}


	private void OnTriggerEnter(Collider col) {
		if(col.tag == "Ladder") {
			if(isCrouching) {
				ladderCollider--;
				UnCrouch();
			}

			ladderCollider++;
		}
	}


	private void OnTriggerExit(Collider col) {
		if(col.tag == "Ladder") {
			ladderCollider--;
		}
	}


	private void OnControllerColliderHit(ControllerColliderHit hit) {
		Rigidbody rb = hit.rigidbody;

		if(rb == null || rb.isKinematic) {
			return;
		}

		if(hit.moveDirection.y < -0.3f) {
			return;
		}

		MoveableObject mo = hit.transform.GetComponent<MoveableObject>();

		if(mo != null) {
			mo.Pushing();
		}

		Vector3 dir = hit.moveDirection;
		dir.y = rb.velocity.y;

		rb.velocity = dir;
	}
}
