﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class EnemyControl : MonoBehaviour {
	public enum State {
		IDLE,
		MOVING,
		CHASING,
		SEARCHING,
		SUSPICIOUS,
		ATTACKING
	}

	public List<Transform> WayPoints = new List<Transform>();
	public float DetectionRange = 20;
	public float DetectionAngle = 120;
	public float ChasingSpeed;
	public float WalkingSpeed;
	public float AttackRange;
	public Transform LaserBeam;
	public Transform GunPoint;
	public Color ColorIdle;
	public Color ColorSuspicious;
	public Color ColorChasing;
	public Renderer GlowRenderer;
	public Transform DeathParticles;

	public State currentState = State.IDLE;

	private Animator animator;
	private NavMeshAgent agent;
	private int wayPointID;
	private Vector3 nextPosition;

	private RaycastHit hit;
	private Ray ray;
	private Vector3 lastPlayerPos;
	private float alertness = 0;
	private float maxAlertness = 2;
	private bool byPassWayPointCheck = false;
	private float shootDelay = 0.7f;
	private float shootTimer = 0;
	private Material glowMaterial;


	private void Start() {
		AlarmManager.OnTriggeredAlarm += AlertEnemy;
		shootTimer = shootDelay;

		animator = GetComponent<Animator>();
		agent = GetComponent<NavMeshAgent>();
		glowMaterial = GlowRenderer.materials[1];

		if(WayPoints.Count == 0) {
			WayPoints.Add(transform);
		}

		Invoke ("NextWayPoint", Random.Range (3, 6));
	}


	private void OnDisable() {
		AlarmManager.OnTriggeredAlarm -= AlertEnemy;
	}


	private void Update() {
		switch(currentState) {
		case State.IDLE:
			glowMaterial.SetColor ("_EmissionColor", ColorIdle);
			animator.SetBool("isWalking", false);
			animator.SetBool("isAlerted", false);
			animator.SetBool("isInAttackRange", false);
			break;

		case State.MOVING:
			glowMaterial.SetColor ("_EmissionColor", ColorIdle);
			animator.SetBool("isWalking", true);
			animator.SetBool("isAlerted", false);
			animator.SetBool("isInAttackRange", false);

			agent.speed = WalkingSpeed;

			if(Vector3.Distance(transform.position, nextPosition) < 1f) {
				currentState = State.IDLE;
				agent.Stop(true);

				Invoke ("NextWayPoint", Random.Range (1, 3));
			}
			break;

		case State.SEARCHING:
			glowMaterial.SetColor ("_EmissionColor", ColorSuspicious);
			animator.SetBool("isWalking", true);
			animator.SetBool("isAlerted", false);
			animator.SetBool("isInAttackRange", false);

			agent.speed = WalkingSpeed;
			agent.SetDestination(lastPlayerPos);

			if(Vector3.Distance(new Vector3(transform.position.x, 0, transform.position.z), new Vector3(lastPlayerPos.x, 0, lastPlayerPos.z)) < 1f) {
				animator.SetBool("isWalking", false);
				animator.SetBool("isAlerted", false);
				animator.SetBool("isInAttackRange", false);
			}

			LowerSuspicion(0.4f);
			break;

		case State.CHASING:
			glowMaterial.SetColor ("_EmissionColor", ColorChasing);
			animator.SetBool("isAlerted", true);
			animator.SetBool("isWalking", true);
			animator.SetBool("isInAttackRange", false);

			agent.speed = ChasingSpeed;

			AlarmManager.ins.ResetAlarmTimer();

			if(CanSeePlayer()) {
				AlarmManager.ins.ResetAlarmTimer();
				agent.SetDestination(lastPlayerPos);
				Vector3 sightingDeltaPos = lastPlayerPos - transform.position;
				
				if(sightingDeltaPos.sqrMagnitude < AttackRange) {
					currentState = State.ATTACKING;
				}
			} else if(Vector3.Distance(new Vector3(transform.position.x, 0, transform.position.z), new Vector3(lastPlayerPos.x, 0, lastPlayerPos.z)) < 1f) {
				lastPlayerPos = PlayerControl.ins.transform.position;
				currentState = State.SEARCHING;
			}
			break;

		case State.SUSPICIOUS:
			glowMaterial.SetColor ("_EmissionColor", ColorSuspicious);
			SmoothLookAt();
			agent.Stop(true);

			animator.SetBool("isWalking", false);
			animator.SetBool("isAlerted", false);
			animator.SetBool("isInAttackRange", false);

			if(CanSeePlayer()) {
				alertness += Time.deltaTime;
			} else {
				LowerSuspicion();
			}

			if(alertness > maxAlertness) {
				AlarmManager.ins.TriggerAlarm(lastPlayerPos, transform);
				currentState = State.CHASING;
			}
			break;

		case State.ATTACKING:
			glowMaterial.SetColor ("_EmissionColor", ColorChasing);
			agent.Stop(true);

			animator.SetBool("isAlerted", false);
			animator.SetBool("isWalking", false);
			animator.SetBool("isInAttackRange", true);

			shootTimer += Time.deltaTime;

			if(!CanSeePlayer()) {
				agent.SetDestination(lastPlayerPos);
				currentState = State.CHASING;
			} else {
				AlarmManager.ins.ResetAlarmTimer();
				SmoothLookAt();

				if(animator.GetFloat("Shot") > 0.5f && shootTimer >= shootDelay) {
					shootTimer = 0;

					ShootLaser();
				}
			}
			break;
		}
	}


	private void ShootLaser() {
		Transform laser = Instantiate (LaserBeam, GunPoint.position, Quaternion.identity) as Transform;
		Vector3 offset = new Vector3(Random.Range(-0.2f, 0.2f), Random.Range(0.4f, 0.6f), Random.Range(-0.2f, 0.2f));
		laser.rotation = Quaternion.LookRotation((PlayerControl.ins.transform.position + offset) - GunPoint.position);
	}


	private void NextWayPoint() {
		if(WayPoints.Count <= 1 && !byPassWayPointCheck)
			return;

		byPassWayPointCheck = false;

		nextPosition = WayPoints[wayPointID].position;

		agent.SetDestination(nextPosition);
		wayPointID++;

		if(wayPointID >= WayPoints.Count) {
			wayPointID -= WayPoints.Count;
		}

		currentState = State.MOVING;
	}


	public void Died() {
		Instantiate (DeathParticles, transform.position, Quaternion.identity);
		Destroy (gameObject);
	}


	public void Hit() {
		AlertEnemy(PlayerControl.ins.transform.position, null);
	}


	public bool CanSeePlayer() {
		Vector3 forward = transform.position - PlayerControl.ins.transform.position;
		float angle = Vector3.Angle(transform.forward, forward);
		
		if(angle > (DetectionAngle * (currentState == State.CHASING ? 0.2f : 1f))) {
			ray = new Ray(transform.position + new Vector3(0, 1.5f, 0), (PlayerControl.ins.CameraPtr.position - new Vector3(0, 0.3f, 0)) - (transform.position + new Vector3(0, 1.5f, 0)));

			Debug.DrawRay(ray.origin, ray.direction);

			if(Physics.Raycast(ray, out hit, DetectionRange * (currentState == State.CHASING ? 3f : 1f))) {
				if(hit.transform.tag == "Player") {
					lastPlayerPos = hit.transform.position;
					return true;
				}
			}
		}

		return false;
	}


	public void AlertEnemy(Vector3 playerPos, Transform triggerer) {
		if(triggerer != transform) {
			lastPlayerPos = playerPos;
			alertness = maxAlertness;
			agent.Stop ();
			agent.SetDestination(lastPlayerPos);
			currentState = State.CHASING;
		}
	}


	private void LowerSuspicion(float modifier = 1f) {
		alertness -= Time.deltaTime * modifier;
		
		if(alertness <= 0) {
			alertness = 0;
			agent.Stop();
			currentState = State.IDLE;

			if(WayPoints.Count == 1)
				byPassWayPointCheck = true;

			Invoke ("NextWayPoint", Random.Range (3, 6));
		}
	}


	private void SmoothLookAt() {
		Vector3 target = lastPlayerPos - transform.position;
		
		Quaternion lookAtRotation = Quaternion.LookRotation(target, Vector3.up);
		
		transform.rotation = Quaternion.Lerp(transform.rotation, lookAtRotation, 4f * Time.deltaTime);
		transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
	}
}
