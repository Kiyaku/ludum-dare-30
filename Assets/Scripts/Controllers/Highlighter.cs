﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Highlighter : MonoBehaviour {
	public static Highlighter ins;

	public UISprite HighlightSprite;
	public UILabel HighlightName;
	public Camera PlayerCam;
	public Camera UICam;
	public UIProgressBar ProgressBar;
	public LayerMask ScanMask;
	[HideInInspector]
	public UseableObject HighlightedObject;


	private RaycastHit hit;
	private Ray ray;


	private void Awake() {
		ins = this;
	}


	private void Update() {
		ray = PlayerCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

		if(Physics.Raycast(ray, out hit, 1.8f, ScanMask)) {
			HighlightObject(hit.transform);
		} else {
			HideHighlighter();
		}
	}


	public void HideProgressBar() {
		ProgressBar.gameObject.SetActive(false);
	}


	public void UpdateProgressBar(float val) {
		ProgressBar.gameObject.SetActive(true);
		ProgressBar.value = val;
	}


	private void HideHighlighter() {
		if(HighlightedObject != null)
			HighlightedObject.Deselect();

		HideProgressBar();
		HighlightSprite.enabled = false;
		HighlightName.enabled = false;
		HighlightedObject = null;
	}


	private void HighlightObject(Transform obj) {
		HighlightedObject = hit.transform.GetComponent<UseableObject>();

		if(HighlightedObject == null) {
			HideHighlighter();
			return;
		}

		HighlightSprite.enabled = true;
		HighlightName.enabled = true;
		HighlightName.text = HighlightedObject.InfoText;

		float leftSide = obj.collider.bounds.extents.magnitude;

		// Get bound offset
		Vector3 leftSidePos = obj.position;
		Vector3 offset = new Vector3(leftSide, 0, 0);
		Quaternion rot = Quaternion.LookRotation(obj.position - PlayerCam.transform.position);
		offset = rot * offset;
		leftSidePos += offset;
		Vector3 leftSideScreenPos = PlayerCam.WorldToScreenPoint(leftSidePos);


		// Position highlighter
		Vector3 screenPos = PlayerCam.WorldToScreenPoint(obj.position);
		Vector3 newPos = UICam.ScreenToWorldPoint(screenPos);
		HighlightSprite.transform.position = newPos;

		int size = (int)(leftSideScreenPos.x - screenPos.x);

		HighlightSprite.width = size + 40;
		HighlightSprite.height = size + 40;
	}
}
