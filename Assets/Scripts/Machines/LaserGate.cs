﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class LaserGate : MonoBehaviour {
	private void OnTriggerEnter(Collider col) {
		if(col.tag == "Player") {
			AlarmManager.ins.ResetAlarmTimer();
			AlarmManager.ins.TriggerAlarm(col.transform.position, transform);
		}
	}
}
