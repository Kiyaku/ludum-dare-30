﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SecurityCamera : MonoBehaviour {
	private enum CameraState {
		IDLE,
		SEARCH,
		ALARM
	}

	public Animation CameraAnimation;
	public Light Spotlight;
	public Renderer Lense;
	public Color ColorAlarm;
	public Color ColorSearch;
	public Color ColorIdle;

	private RaycastHit hit;
	private Ray ray;
	private Vector3 lastPlayerPos;
	private Transform player;
	private Quaternion lastRotation;
	private float animTime;
	private float cameraRange = 12;
	private Material cameraLenseMat;
	private CameraState currentState = CameraState.IDLE;
	private bool isActive = true;


	private void Awake() {
		cameraLenseMat = Lense.materials[0];
	}


	private void Update() {
		if(!isActive)
			return;

		switch(currentState) {
			case CameraState.IDLE:
				Spotlight.color = ColorIdle;
				cameraLenseMat.SetColor("_EmissionColor", ColorIdle);

				if(!CameraAnimation.isPlaying) {
					CameraAnimation.transform.rotation = Quaternion.Lerp(CameraAnimation.transform.rotation, lastRotation, Time.deltaTime);

					if(Quaternion.Angle(lastRotation, CameraAnimation.transform.rotation) < 5f) {
						CameraAnimation.Play();
						CameraAnimation["cameraSwing"].time = animTime;
					}
				} else {
					lastRotation = CameraAnimation.transform.rotation;
					animTime = CameraAnimation["cameraSwing"].time;
				}
				break;

			case CameraState.SEARCH:
				Spotlight.color = ColorSearch;
				cameraLenseMat.SetColor("_EmissionColor", ColorSearch);

				Invoke("GoBackToIdle", 5);
				break;

			case CameraState.ALARM:
				CancelInvoke("GoBackToIdle");
				Spotlight.color = ColorAlarm;
				cameraLenseMat.SetColor("_EmissionColor", ColorAlarm);

				ray = new Ray(transform.position, player.position - transform.position);

				if(Physics.Raycast(ray, out hit, cameraRange)) {
					if(hit.transform.tag == "Player") {
						lastPlayerPos = hit.transform.position;
					} else {
						currentState = CameraState.SEARCH;
					}
				} else {
					currentState = CameraState.SEARCH;
				}

				CameraAnimation.Stop();

				SmoothLookAt();
				break;
		}
	}


	public void DisableCamera() {
		isActive = false;
		Spotlight.enabled = false;
		cameraLenseMat.SetColor("_EmissionColor", Color.black);
		CameraAnimation.Stop();
	}


	public void EnableCamera() {
		isActive = true;
		Spotlight.enabled = true;
	}


	private void GoBackToIdle() {
		currentState = CameraState.IDLE;
	}


	private void OnTriggerStay(Collider col) {
		if(!isActive)
			return;

		if(col.tag == "Player") {
			ray = new Ray(transform.position, col.transform.position - transform.position);

			if(Physics.Raycast(ray, out hit, cameraRange)) {
				if(hit.transform.tag == "Player") {
					AlarmManager.ins.ResetAlarmTimer();
					player = hit.transform;
					currentState = CameraState.ALARM;
					AlarmManager.ins.TriggerAlarm(player.position, transform);
				}
			}
		}
	}


	private void SmoothLookAt() {
		Vector3 target = lastPlayerPos - transform.position;

		Quaternion lookAtRotation = Quaternion.LookRotation(target);

		CameraAnimation.transform.rotation = Quaternion.Lerp(CameraAnimation.transform.rotation, lookAtRotation, 1f * Time.deltaTime);
	}
}
