﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Door : MonoBehaviour {
	public int DoorSpeed;
	public Transform DoorMesh;
	public bool IsLocked;
	public string RequiredKeyCard;
	public List<string> AllowedEntities = new List<string>();

	public Color ColorUnlocked;
	public Color ColorLocked;
	public Color ColorKeycard;

	private bool isOpen = false;
	private Vector3 startPos;


	private void Awake() {
		startPos = DoorMesh.localPosition;
	}


	private void Update() {
		DoorMesh.renderer.material.SetColor("_EmissionColor", !IsLocked ? (RequiredKeyCard != "" ? ColorKeycard : ColorUnlocked) : ColorLocked);
	}


	private IEnumerator OpenDoor() {
		float timer = 0;
		Vector3 newPos = startPos + new Vector3(2, 0, 0);

		while(timer < 1) {
			DoorMesh.localPosition = Vector3.Lerp(startPos, newPos, timer);

			timer += Time.deltaTime * DoorSpeed;
			yield return null;
		}

		isOpen = true;
		DoorMesh.localPosition = newPos;
	}


	private IEnumerator CloseDoor() {
		float timer = 0;
		Vector3 curPos = DoorMesh.localPosition;
		
		while(timer < 1) {
			DoorMesh.localPosition = Vector3.Lerp(curPos, startPos, timer);
			
			timer += Time.deltaTime * DoorSpeed;
			yield return null;
		}

		isOpen = false;
		DoorMesh.localPosition = startPos;
	}


	public void UnlockDoor() {
		IsLocked = false;
	}


	public void LockDoor() {
		IsLocked = true;
	}


	private void OnTriggerEnter(Collider col) {
		// TODO: needs to work on any humanoid
		if(AllowedEntities.Contains (col.tag)) {
			if(!isOpen && !IsLocked) {
				Inventory inv = col.GetComponent<Inventory>();

				if((RequiredKeyCard != "" && inv != null && inv.HasItem(RequiredKeyCard)) || RequiredKeyCard == "") {
					StopCoroutine("CloseDoor");
					StartCoroutine("OpenDoor");
				}
			}
		}
	}


	private void OnTriggerExit(Collider col) {
		// TODO: needs to work on any humanoid
		if(AllowedEntities.Contains (col.tag)) {
			if(isOpen) {
				StopCoroutine("OpenDoor");
				StartCoroutine("CloseDoor");
			}
		}
	}
}
