﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class AlarmLight : MonoBehaviour {
	public Light[] Spotlights = new Light[2];

	private bool alarmTriggered = false;


	private void Start() {
		AlarmManager.OnTriggeredAlarm += TriggerLights;

		foreach(Light l in Spotlights)
			l.enabled = false;
	}


	private void OnDisable() {
		AlarmManager.OnTriggeredAlarm -= TriggerLights;
	}


	private void TriggerLights(Vector3 lastPos, Transform transform) {
		foreach(Light l in Spotlights)
			l.enabled = true;

		alarmTriggered = true;
	}


	private void Update() {
		if(alarmTriggered)
			transform.Rotate(Vector3.right * 100f * Time.deltaTime);
	}
}
