﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class LightBridge : MonoBehaviour {
	private float tintAlpha;
	private Color tintColor;

	private void Awake() {
		tintColor = renderer.material.GetColor("_TintColor");
		tintAlpha = tintColor.a;
	}


	private void Update() {
		renderer.material.SetColor("_TintColor", tintColor + new Color(0, 0, 0, tintAlpha + Random.Range(-0.1f, 0.1f)));
	}
}
