﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ItemDB : MonoBehaviour {
	public static ItemDB ins;

	public List<Item> ItemList = new List<Item>();


	private void Awake() {
		ins = this;
	}


	public Item GetItemByID(string itemID) {
		return ItemList.Find (i => i.ID == itemID);
	}
}
