﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Barrel : MonoBehaviour {
	public void Died() {
		collider.enabled = false;

		Collider[] cols = Physics.OverlapSphere(transform.position, 6);

		foreach(Collider col in cols) {
			Health h = col.GetComponent<Health>();

			if(h != null) {
				h.ReceivedDamage(200);
			}
		}

		Destroy (gameObject);
	}
}
