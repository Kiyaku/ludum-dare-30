﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public static class Helper {
	public static Vector3 NIRVANA = new Vector3(10000, 10000, 10000);

	public static float ClampAngle(float angle, float min, float max) {
		if(min < 0)
			min += 360;

		if(angle > 180) {
			return Mathf.Clamp(angle, min, 360);
		} else {
			return Mathf.Clamp(angle, 0, max);
		}
	}


	// Note that Color32 and Color implictly convert to each other. You may pass a Color object to this method without first casting it.
	public static string ColorToHex(Color32 color)
	{
		string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
		return hex;
	}
	
	public static Color HexToColor(string hex)
	{
		byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
		return new Color32(r,g,b, 255);
	}
}
