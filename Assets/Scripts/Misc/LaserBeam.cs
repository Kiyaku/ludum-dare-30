﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class LaserBeam : MonoBehaviour {
	public List<string> IgnoredTags = new List<string>();
	public int Damage;
	public float Speed = 20;

	private Vector3 lastPos;
	private RaycastHit hit;
	private Ray ray;


	private void Awake() {
		lastPos = transform.position;
		Invoke ("DeleteLaser", 5);
	}


	private void DeleteLaser() {
		Destroy (gameObject);
	}


	private void Update() {
		transform.Translate(Vector3.forward * Speed * Time.deltaTime);

		ray = new Ray(transform.position, lastPos - transform.position);
		float length = Vector3.Distance(transform.position, lastPos);

		if(Physics.Raycast(ray, out hit, length)) {
			if(!IgnoredTags.Contains(hit.transform.tag)) {
				Health health = hit.transform.GetComponent<Health>();

				if(health != null) {
					health.ReceivedDamage(Damage);
				}

				DeleteLaser();
			}
		}

		lastPos = transform.position;
	}
}
