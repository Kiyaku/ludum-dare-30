﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Health : MonoBehaviour {
	public int MaxHealth = 10;
	public UISprite HealthPointPrefab;
	public GameObject HealthbarAnchor;
	public List<Color> HealthColors = new List<Color>() {new Color(219f / 255f, 42f / 255f, 122f / 255f)};

	private int currentHealth;
	private List<UISprite> HealthPoints = new List<UISprite>();


	private void Awake() {
		currentHealth = MaxHealth;

		if(HealthColors.Count == 0) {

		}

		if(HealthbarAnchor == null)
			return;

		ChangeHeartCount();
	}


	public void SetMaxHealth(int max) {
		MaxHealth = max;
		ChangeHeartCount();
	}


	private void ChangeHeartCount() {
		int x = 0;

		for(int i = 0; i < MaxHealth; i++) {
			if(HealthPoints.Count <= i) {
				GameObject healthPoint = NGUITools.AddChild(HealthbarAnchor, HealthPointPrefab.gameObject);
				healthPoint.transform.localPosition = new Vector3(-91 + 20 * x, (i % 2 == 0 ? -1 : 11), 0);
				healthPoint.GetComponent<UISprite>().color = GetColorForHeart(i);
				HealthPoints.Add(healthPoint.GetComponent<UISprite>());
			}
			
			x++;
			
			if(x >= 10)
				x -= 10;
		}

		UpdateHealthDisplay();
	}


	private Color GetColorForHeart(int index) {
		if(HealthColors.Count == 0) {
			Debug.LogWarning("No colors for health set", transform);
			return Color.white;
		}

		int i = index / 10;

		while(HealthColors.Count <= i) {
			i -= HealthColors.Count;
		}

		return HealthColors[i];
	}


	public void Gainhealth(int health) {
		currentHealth += health;
	
		currentHealth = Mathf.Clamp (currentHealth, 0, MaxHealth);

		UpdateHealthDisplay();
	}


	public void ReceivedDamage(int dmg) {
		dmg = Mathf.Clamp(dmg, 0, dmg);
		
		currentHealth -= dmg;

		if(HealthPointPrefab != null) {
			UpdateHealthDisplay();
			StartCoroutine(AnimateHearts());
		}

		if(currentHealth <= 0) {
			transform.SendMessage("Died", SendMessageOptions.DontRequireReceiver);
		} else {
			transform.SendMessage("Hit", SendMessageOptions.DontRequireReceiver);
		}
	}


	private void UpdateHealthDisplay() {
		if(HealthbarAnchor != null) {
			int healthLayer = currentHealth / 10;
			
			for(int i = 0; i < MaxHealth; i++) {
				int heartLayer = i / 10;
				
				HealthPoints[i].gameObject.SetActive(i < currentHealth && i + 10 >= currentHealth);
			}
		}
	}


	private IEnumerator AnimateHearts() {
		float timer = 0;
		int x = 0;
		int index;

		while(timer < 1) {
			timer += Time.deltaTime * 5f;

			index = currentHealth - 10;

			if(index < 0)
				index = 0;
			
			x = (index) % 10;

			for(int i = index; i < currentHealth; i++) {
				Vector3 pos = new Vector3(-91 + 20 * x, (i % 2 == 0 ? -1 : 11), 0);
				pos.x += Random.Range (-2f, 2f);
				pos.y += Random.Range (-2f, 2f);

				HealthPoints[i].transform.localPosition = pos;
				x++;
				
				if(x >= 10)
					x -= 10;
			}

			yield return null;
		}

		index = currentHealth - 10;
		
		if(index < 0)
			index = 0;
		
		x = (index) % 10;
		
		for(int i = index; i < currentHealth; i++) {
			Vector3 pos = new Vector3(-91 + 20 * x, (i % 2 == 0 ? -1 : 11), 0);
			
			HealthPoints[i].transform.localPosition = pos;
			x++;
			
			if(x >= 10)
				x -= 10;
		}
	}
}
