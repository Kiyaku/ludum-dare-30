﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Item {
	public enum Types {
		KEY,
		CONSUMABLE,
		WEAPON
	}

	public string ID;
	public Types Type;
	public string Name;
	public string Description;
	public Texture2D Image;
	public Transform WorldPrefab;
}
