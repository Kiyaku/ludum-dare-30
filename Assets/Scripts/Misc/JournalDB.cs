﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class JournalDB : MonoBehaviour {
	public static JournalDB ins;

	public List<Journal> Journals = new List<Journal>();
	public List<string> FoundJournalIDs = new List<string>();


	private void Awake() {
		ins = this;
	}


	public void AddToJournal(string journalID) {
		if(!FoundJournalIDs.Contains(journalID))
			FoundJournalIDs.Add(journalID);
	}


	public Journal GetJournalByID(string id) {
		return Journals.Find(j => j.ID == id);
	}
}
