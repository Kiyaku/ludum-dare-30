﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class UseableObject : MonoBehaviour {
	public string InfoText;


	protected virtual void Awake() {
		if(InfoText == "")
			InfoText = transform.name;
	}


	public virtual void Trigger() {
	}


	public void Deselect() {
		StopAllCoroutines();
	}


	protected IEnumerator StartProcess(Action onFinished, float time) {
		float timer = 0;

		while(timer < time) {
			Highlighter.ins.UpdateProgressBar(timer / time);
			timer += Time.deltaTime;

			yield return null;
		}

		if(onFinished != null)
			onFinished();

		Highlighter.ins.HideProgressBar();
	}
}
