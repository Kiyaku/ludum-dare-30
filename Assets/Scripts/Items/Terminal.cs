﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Terminal : UseableObject {
	public bool IsActivated = false;
	public Activator MyActivator;
	public int SecurityLevel = 0;
	public int MultitoolsRequired = 0;
	public string KeyCodeRequired = "";
	public string UnlockText = "Door Unlocked";

	private bool isUnlocked = false;


	protected override void Awake () {
		base.Awake ();

		UpdateStatus();
	}


	private void UpdateStatus() {
		if(MultitoolsRequired == 0)
			isUnlocked = true;

		if(isUnlocked) {
			InfoText = "Terminal - Access Granted";
			InfoText += "\n" + UnlockText + ": " + IsActivated;
		} else {
			InfoText = "Terminal - Security Level " + SecurityLevel;

			if(KeyCodeRequired != "")
				InfoText += "\nPassword required";
			else
				InfoText += "\nMultitools required: " + MultitoolsRequired;
		}
	}


	public override void Trigger ()	{
		base.Trigger ();

		if(isUnlocked) {
			ToggleStatus();
		} else {
			if(KeyCodeRequired != "") {
				KeyCodePanel.ins.OpenPanel(transform.name, KeyCodeRequired, Unlock);
				PanelManager.Ins.ShowPanel("[Panel] KeyCodePanel");
			} else if(PlayerControl.ins.PlayerInventory.HasItem("multitool")) {
				StartCoroutine(StartProcess(ReduceSecuritylevel, 5));
			}
		}
	}


	private void Unlock() {
		isUnlocked = true;
		ToggleStatus();
	}


	private void ReduceSecuritylevel() {
		if(PlayerControl.ins.PlayerInventory.HasItem("multitool")) {
			PlayerControl.ins.PlayerInventory.RemoveItem("multitool", 1);
			MultitoolsRequired--;

			if(MultitoolsRequired == 0) {
				ToggleStatus();
				isUnlocked = true;
			}

			UpdateStatus();
		}
	}


	private void ToggleStatus() {
		if(IsActivated) {
			if(MyActivator != null) {
				MyActivator.Deactivate();
			}
		} else {
			if(MyActivator != null) {
				MyActivator.Activate();
			}
		}

		IsActivated = !IsActivated;
		UpdateStatus();
	}
}
