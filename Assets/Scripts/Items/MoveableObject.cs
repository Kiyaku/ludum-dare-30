﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class MoveableObject : UseableObject {
	public AudioClip SFXImpact;
	public AudioClip SFXPushing;

	private Color startColor;
	private bool isTossing = false;
	private bool isBeingPushed = false;

	
	protected override void Awake ()
	{
		base.Awake ();
		
		startColor = renderer.material.color;
	}


	private void SetStatus(bool carried) {
		rigidbody.isKinematic = carried;
		rigidbody.useGravity = !carried;
		collider.enabled = !carried;
		startColor.a = carried ? 0.7f : 1f;
		renderer.material.color = startColor;
	}


	public override void Trigger () {
		base.Trigger ();

		float colSize = (collider as BoxCollider).size.x * transform.localScale.x;
		
		SetStatus (true);
		PlayerControl.ins.CarryObject(transform, colSize);
	}


	public void DropObject() {
		SetStatus(false);
	}

	public void TossObject() {
		SetStatus(false);

		isTossing = true;
	}


	public void Pushing() {
		//isBeingPushed = true;
	}


	private void FixedUpdate() {
		if(isTossing) {
			isTossing = false;

			Vector3 target = transform.position - (PlayerControl.ins.CameraPtr.position + new Vector3(0, -0.6f, 0));
			rigidbody.AddForce(target.normalized * 500f);
		}

		if(isBeingPushed) {
			if(!audio.isPlaying) {
				audio.pitch = 1;
				audio.clip = SFXPushing;
				audio.Play();
			}

			isBeingPushed = false;
		} else if(audio.clip == SFXPushing) {
			audio.clip = null;
			audio.Stop();
		}
	}


	private void OnCollisionEnter(Collision col) {
		if(col.relativeVelocity.magnitude >= 3) {
			audio.pitch = Random.Range(0.7f, 0.9f);
			audio.PlayOneShot(SFXImpact);
		}
	}


	private void OnCollisionStay(Collision col) {
		if(col.relativeVelocity.magnitude >= 4.5f) {
			audio.pitch = Random.Range(0.7f, 0.9f);
			audio.PlayOneShot(SFXImpact);
		}
	}
}
