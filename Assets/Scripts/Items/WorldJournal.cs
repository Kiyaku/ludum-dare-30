using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class WorldJournal : UseableObject {
	public string JournalID;


	public override void Trigger ()	{
		base.Trigger ();

		Journal j = JournalDB.ins.GetJournalByID(JournalID);

		JournalDB.ins.AddToJournal(j.ID);
		PlayerControl.ins.GetComponent<Inventory>().AddJournal(JournalID);

		IngameJournalUI.ins.OpenJournal(j);
	}
}
