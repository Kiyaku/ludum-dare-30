﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Journal {
	public enum Types {
		TASK,
		MEMO
	}

	public string ID;
	public string Title;
	public string Text;
	public Types Type = Types.MEMO;
}
