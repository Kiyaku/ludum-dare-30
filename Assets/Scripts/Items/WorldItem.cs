﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class WorldItem : UseableObject {
	public string ItemID;
	public int Quantity;


	private void Start() {
		transform.name = ItemDB.ins.GetItemByID(ItemID).Name;
		InfoText = transform.name;
	}


	public override void Trigger() {
		base.Trigger ();

		bool successful = PlayerControl.ins.GetComponent<Inventory>().AddItem(ItemID, Quantity);

		if(successful)
			Destroy (gameObject);
		else 
			Debug.Log("Inventory full!");
	}
}
